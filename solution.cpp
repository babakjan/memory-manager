#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <pthread.h>
#include <semaphore.h>
#include "common.h"

using namespace std;
#endif /* __PROGTEST__ */

//global stack
uint32_t *freePages;  //stack of indexes of free (unused) pages
size_t freePagesSize;  //size of stack (how many free pages there are)
size_t freePagesAllocatedSize;  //allocated size of stack
pthread_mutex_t freePagesMutex;  //lock the variables above
pthread_cond_t condEnd;  //if main function can end

//controllers of number of running processes
uint32_t numberOfRunningProcesses = 0;
pthread_mutex_t numberOfRunningProcessesMutex;

//struct used for passing arguments to new thread
typedef struct CThreadArguments {
    CCPU *cpu;
    void *processArg;

    void (*entryPoint)(CCPU *, void *);
} CThreadArguments;

//this function runs in new thread
void *threadFunc(void *args) {
    printf("[thread func] start\n");

    CThreadArguments *myArgs = (CThreadArguments *) args;

    //increment counter of running processes
    pthread_mutex_lock(&numberOfRunningProcessesMutex);
    numberOfRunningProcesses++;
    pthread_mutex_unlock(&numberOfRunningProcessesMutex);

    //start new process
    printf("[thread func] start new process\n");
    myArgs->entryPoint(myArgs->cpu, myArgs->processArg);
    printf("[thread func] new process end\n");

    //decrement counter of running processes
    pthread_mutex_lock(&numberOfRunningProcessesMutex);
    numberOfRunningProcesses--;
    pthread_mutex_unlock(&numberOfRunningProcessesMutex);

    //notify main thread that process has end
    pthread_cond_broadcast(&condEnd);

    //clean up
    delete myArgs->cpu;
    delete myArgs;
    printf("[thread func] end\n");

    return NULL;
}

class CMyCPU : public CCPU {
public:

    /**
     * constructor
     * @param memStart
     * @param pageTableRoot index of root page table
     * @param countOfPagesForUsage - total count of pages = countOfPagesForUse + 1 (+1 because root table)
     */
    CMyCPU(uint8_t *memStart, uint32_t pageTableRoot) : CCPU(memStart, pageTableRoot) {
        //initialize root table
        //fill root table by zeros
        for (uint32_t i = 0; i < PAGE_DIR_ENTRIES; ++i) {
            uint32_t *addr =
                    (uint32_t *) (m_MemStart + (pageTableRoot & ADDR_MASK)) + i;  //address of entry in rootTable
            *addr = 0;  //fill entry
        }
        m_CountOfUsedPages = 0;  //at the beginning there are no used pages (root table and subTables does not count)
        m_LastAddedSubTable = NOTEXISTS;  //index of last added entry of subTable in root table (valid entries are from 0 - 1023)

        //initialize attributes for new thread
        pthread_attr_init(&m_ThreadAttributes);
        pthread_attr_setdetachstate(&m_ThreadAttributes, PTHREAD_CREATE_DETACHED);
    };

    //destructor
    ~CMyCPU() {
        pthread_attr_destroy(&m_ThreadAttributes);
    }

    /**
     * @return number of pages used by cpu
     */
    virtual uint32_t GetMemLimit(void) const {
        return m_CountOfUsedPages;
    }

    virtual bool SetMemLimit(uint32_t pages) {

        //memory stay the same
        if (m_CountOfUsedPages == pages) {
            return true;  //no pages need to be added or removed
        }

        //increase memory
        if (m_CountOfUsedPages < pages) {
            //there are already some subTables, fill the last subTable, if there left some space
            if (m_LastAddedSubTable != NOTEXISTS) {
                uint32_t *entryOfLastAddedSubPageInRootTable =
                        (uint32_t *) (m_MemStart + (m_PageTableRoot & ADDR_MASK)) + m_LastAddedSubTable;
                uint32_t lastSubTable = *entryOfLastAddedSubPageInRootTable & ADDR_MASK;

                //add pages to last subTable, if there is free space
                for (uint32_t i = 0; i < PAGE_DIR_ENTRIES && m_CountOfUsedPages < pages; ++i) {
                    uint32_t *entry = (uint32_t *) (m_MemStart + lastSubTable) + i;
                    //entry is 0 => fill it by new page entry
                    if (*entry == 0) {
                        pthread_mutex_lock(&freePagesMutex);
                        if (freePagesSize > 0) {
                            *entry = (freePages[freePagesSize - 1] << OFFSET_BITS) +
                                     7;  //+7 set last 3 bits (U, W, P) to 1
                            freePagesSize--;
                            m_CountOfUsedPages++;
                        } else {
                            return false;  //memory exceeded
                        }
                        pthread_mutex_unlock(&freePagesMutex);
                    }
                }
            }

            //now all subTables are full, if I want to add another pages, I need to create new subTables sor them
            while (m_CountOfUsedPages < pages) {
                //create new subPage
                uint32_t *entryAboutNewSubTable;
                pthread_mutex_lock(&freePagesMutex);
                if (freePagesSize > 0) {
                    uint32_t newSubTable =
                            (freePages[freePagesSize - 1] << OFFSET_BITS) + 7;  //pop new free page for subTable
                    freePagesSize--;
                    entryAboutNewSubTable =
                            (uint32_t *) (m_MemStart + (m_PageTableRoot & ADDR_MASK)) + ++m_LastAddedSubTable;
                    *entryAboutNewSubTable = newSubTable;  //add entry about new subTable to rootTable
                } else {
                    return false;  //memory exceeded
                }
                pthread_mutex_unlock(&freePagesMutex);

                uint32_t lastSubTableAddr = *entryAboutNewSubTable & ADDR_MASK;  //address of last added subTable

                //fill new created subTable by new pages
                for (uint32_t i = 0; i < PAGE_DIR_ENTRIES; ++i) {
                    uint32_t *entry = (uint32_t *) (m_MemStart + lastSubTableAddr) + i;

                    //pages need to be added
                    if (m_CountOfUsedPages < pages) {
                        pthread_mutex_lock(&freePagesMutex);
                        if (freePagesSize > 0) {
                            *entry = (freePages[freePagesSize - 1] << OFFSET_BITS) +
                                     7;  //+7 set last 3 bits (U, W, P) to 1
                            freePagesSize--;
                            m_CountOfUsedPages++;
                        } else {
                            return false;  //memory exceeded
                        }
                        pthread_mutex_unlock(&freePagesMutex);
                    }
                        //pages are added, fill rest of tables by zeros
                    else {
                        *entry = 0;
                    }
                }
            }
            return true;
        }

        //decrease memory - remove pageDifference pages (pageDifference < 0)
        //start removing entries from last subTable
        while (m_CountOfUsedPages > pages) {

            //if there is only root table (subTables not exist), memory can not be decreased any more
            if (m_LastAddedSubTable == NOTEXISTS) {
                return false;
            }

            uint32_t *entryOfLastAddedSubPageInRootTable =
                    (uint32_t *) (m_MemStart + (m_PageTableRoot & ADDR_MASK)) + m_LastAddedSubTable;
            uint32_t lastSubTableAddr = *entryOfLastAddedSubPageInRootTable & ADDR_MASK;

            //remove pages from last subTable - start from last entry to the beginning
            int i = PAGE_DIR_ENTRIES - 1;
            for (; i >= 0 && m_CountOfUsedPages > pages; --i) {
                uint32_t *entry = (uint32_t *) (m_MemStart + lastSubTableAddr) + (uint32_t) i;
                //entry is not 0 => entry is valid => remove it
                if (*entry != 0) {
                    //remove page
                    uint32_t pageToRemoveId = ((*entry) & ADDR_MASK) >> OFFSET_BITS;
                    *entry = 0;  //set page entry to empty entry

                    //return page back to the stack
                    pthread_mutex_lock(&freePagesMutex);
                    if (freePagesSize < freePagesAllocatedSize) {
                        freePages[freePagesSize++] = pageToRemoveId;
                    } else {
                        return false;  //should never happened, freePage stack should have enough space for all pages
                    }
                    pthread_mutex_unlock(&freePagesMutex);
                    m_CountOfUsedPages--;
                }
            }
            //if whole subTable (the last one) is now empty => remove it
            if (i < 0) {
                //remove subTable
                uint32_t pageToRemoveId = lastSubTableAddr >> OFFSET_BITS;
                *entryOfLastAddedSubPageInRootTable = 0;
                m_LastAddedSubTable--;

                //return page back to the stack
                pthread_mutex_lock(&freePagesMutex);
                if (freePagesSize < freePagesAllocatedSize) {
                    freePages[freePagesSize++] = pageToRemoveId;
                } else {
                    return false;  //should never happened, freePage stack should have enough space for all pages
                }
                pthread_mutex_unlock(&freePagesMutex);
            }
        }

        return true;
    }

    /**
     * create new process, only PROCESS_MAX=64 processes can run at the same time
     * @param processArg - initialize arg???
     * @param entryPoint - function in new process
     * @param copyMem - if new process will have empty memory or copy of parents memory
     * @return
     */
    virtual bool NewProcess(void *processArg, void (*entryPoint)(CCPU *, void *), bool copyMem) {
        //check if new process can be created
        pthread_mutex_lock(&numberOfRunningProcessesMutex);
        if (numberOfRunningProcesses >= PROCESS_MAX) {
            pthread_mutex_unlock(&numberOfRunningProcessesMutex);
            return false;
        }
        pthread_mutex_unlock(&numberOfRunningProcessesMutex);

        //pop new page from stack
        pthread_mutex_lock(&freePagesMutex);
        uint32_t rootTableForChildCpu;
        if (freePagesSize > 0) {
            rootTableForChildCpu = freePages[freePagesSize - 1];
            freePagesSize--;
            pthread_mutex_unlock(&freePagesMutex);
        } else {
            pthread_mutex_unlock(&freePagesMutex);
            return false;  //stack limit of free pages exceeded
        }

        rootTableForChildCpu =
                (rootTableForChildCpu << OFFSET_BITS) + 7;  //+7 means that last free bits (present, write, user) are 1

        //create new childCpu cpu
        CMyCPU *childCpu = new CMyCPU(m_MemStart, rootTableForChildCpu);
        if (copyMem) {
            //allocate memory for new process
            if (!(childCpu->SetMemLimit(this->GetMemLimit()))) {
                delete childCpu;
                return false;
            }

            //copy memory
            for (uint32_t i = 0; i <= m_LastAddedSubTable; ++i) {
                uint32_t *entryInRootTable = (uint32_t *) (m_MemStart + (m_PageTableRoot & ADDR_MASK)) + i;
                uint32_t subTableAddr = *entryInRootTable & ADDR_MASK;

                uint32_t *entryInChildRootTable = (uint32_t *)(m_MemStart + (rootTableForChildCpu & ADDR_MASK)) + i;
                uint32_t subTableInChildAddr = * entryInChildRootTable & ADDR_MASK;

                for (uint32_t j = 0; j < PAGE_DIR_ENTRIES; ++j) {
                    uint32_t *entryInSubTable = (uint32_t *) (m_MemStart + subTableAddr) + j;
                    uint32_t pageAddr = *entryInSubTable & ADDR_MASK;

                    uint32_t *entryInChildSubTable = (uint32_t *) (m_MemStart + subTableInChildAddr) + j;
                    uint32_t childPageAddr = * entryInChildSubTable & ADDR_MASK;

                    //copy content of current page
                    for (uint32_t k = 0; k < PAGE_DIR_ENTRIES; ++k) {
                        uint32_t *rowInPage = (uint32_t *) (m_MemStart + pageAddr) + k;
                        uint32_t *rowInChildPage = (uint32_t *) (m_MemStart + childPageAddr) + k;
                        *rowInChildPage = *rowInPage;
                    }
                }
            }
        }

        //create thread arguments structure
        CThreadArguments *arguments = new CThreadArguments;
        arguments->cpu = childCpu;
        arguments->processArg = processArg;
        arguments->entryPoint = entryPoint;

        //start new thread
        pthread_t thread;
        if (pthread_create(&thread, &m_ThreadAttributes, threadFunc, (void *) arguments)) {
            delete arguments;
            delete childCpu;
            return false;
        }
        return true;
    }

    uint32_t m_CountOfUsedPages;  //count of used pages (does not count root table and subTables)
    uint32_t m_LastAddedSubTable;  //index of entry of last added subTable - valid is from 0 to 1023
    pthread_attr_t m_ThreadAttributes;  //arguments for new threads - thread are detach

    //if lastAddedSubTable == NOTEXISTS means, that there are no subTables, only root table
    static const uint32_t NOTEXISTS = -1;

protected:
    /*
     if copy-on-write is implemented:

     virtual bool             pageFaultHandler              ( uint32_t          address,
                                                              bool              write );
     */
};


/**
 * simulation start
 * @param mem - memory - pointer to its start
 * @param totalPages - size of memory - number of pages (size of one page is 4 KiB)
 * @param processArg -
 * @param mainProcess - init function - create inner structures
 */
void MemMgr(void *mem, uint32_t totalPages, void *processArg, void (*mainProcess)(CCPU *, void *)) {

    //initialize stack of free pages
    freePagesSize = totalPages;
    freePagesAllocatedSize = totalPages;
    freePages = new uint32_t[freePagesSize];
    pthread_mutex_init(&freePagesMutex, NULL);
    //put ids of all pages into stack
    for (size_t i = 0; i < freePagesSize; ++i) {
        freePages[i] = i;
    }

    pthread_mutex_init(&numberOfRunningProcessesMutex, NULL);
    pthread_cond_init(&condEnd, NULL);

    //create root table
    uint32_t pageTableRoot = freePages[freePagesSize - 1];  //pop last element from stack
    freePagesSize--;
    pageTableRoot =
            (pageTableRoot << CCPU::OFFSET_BITS) + 7;  //+7 means that last free bits (present, write, user) are 1

    //create cpu
    CCPU *myCpu = new CMyCPU((uint8_t *) mem, pageTableRoot);

    //start process
    mainProcess(myCpu, processArg);

    //wait until all processes end
    while (numberOfRunningProcesses > 0) {
        pthread_cond_wait(&condEnd, &numberOfRunningProcessesMutex);
    }

    //clean up
    pthread_mutex_destroy(&freePagesMutex);
    pthread_mutex_destroy(&numberOfRunningProcessesMutex);
    pthread_cond_destroy(&condEnd);
    delete[] freePages;
    delete myCpu;
}
