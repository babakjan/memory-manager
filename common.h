#ifndef __common_h__5872395623940562390452903457234__
#define __common_h__5872395623940562390452903457234__

const uint32_t PROCESS_MAX = 64;

class CCPU {
public:
    static const uint32_t OFFSET_BITS = 12;  //number of bits used for page addressation
    static const uint32_t PAGE_SIZE = 1 << OFFSET_BITS;  //size of page in bytes
    static const uint32_t PAGE_DIR_ENTRIES = PAGE_SIZE / 4;  //number of entries in page directory
    static const uint32_t ADDR_MASK = ~(PAGE_SIZE - 1);  //
    static const uint32_t BIT_PRESENT = 0x0001;  //if page is present (1) or saved on disk (0)
    static const uint32_t BIT_WRITE = 0x0002;  //if page is readonly (0) or read and write (1)
    static const uint32_t BIT_USER = 0x0004;  //set if user (1) have access for page or only supervisor (0)
    static const uint32_t BIT_REFERENCED = 0x0020;  //set in time or reading from page
    static const uint32_t BIT_DIRTY = 0x0040;  //set in time of writing into page

    /**
     * constructor
     * @param memStart "real" pointer to memory start
     * @param pageTableRoot - ramec stranky
     */
    CCPU(uint8_t *memStart, uint32_t pageTableRoot);

    /**
     * destructor
     */
    virtual ~CCPU(void) {}

    /**
     * @return number of pages, which has allocated current process
     */
    virtual uint32_t GetMemLimit(void) const = 0;

    /**
     * set number of pages limit for current process, can be used for scaling up or down memory space
     * @param pages - number of pages
     * @return true if success, otherwise false
     */
    virtual bool SetMemLimit(uint32_t pages) = 0;

    /**
     * create new simulated process
     * @param processArg
     * @param entryPoint
     * @param copyMem
     * @return
     */
    virtual bool NewProcess(void *processArg, void (*entryPoint)(CCPU *, void *), bool copyMem) = 0;

    /**
     * read value of from address
     * @param address - virtual addresses are multiplications of 4
     * @param value
     * @return - true if success, otherwise false
     */
    bool ReadInt(uint32_t address, uint32_t &value);

    /**
     * write value to address
     * @param address - virtual addresses are multiplications of 4
     * @param value
     * @return true if success, otherwise false
     */
    bool WriteInt(uint32_t address, uint32_t value);

protected:

    /**
     * convert virtual address in process to "physical" address
     * @param address
     * @param write
     * @return
     */
    uint32_t *virtual2Physical(uint32_t address, bool write);

    /**
     * this method is called when fail occurred - for example page not found
     * @param address
     * @param write
     * @return - true if problem solved (e.g. page found in swap space), otherwise false
     */
    virtual bool pageFaultHandler(uint32_t address,
                                  bool write) {
        return false;
    }

    uint8_t *m_MemStart;
    uint32_t m_PageTableRoot;
};


void MemMgr(void *mem,
            uint32_t totalPages,
            void *processArg,
            void           (*mainProcess)(CCPU *, void *));

#endif /* __common_h__5872395623940562390452903457234__ */
